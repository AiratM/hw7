﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HW_7.Entities;

namespace HW_7.Interfaces
{
    public interface IClientReader
    {
        /// <summary>
        /// Загрузка клиентов
        /// </summary>
        /// <param name="count">Количество для загрузки</param>
        /// <param name="clients">Коллекция клиентов</param>
        /// <param name="offset">Смещение</param>
        /// <returns>true - ok</returns>
        bool ReadClients(int count, out List<Client> clients, int offset = 0);
        
        /// <summary>
        /// Файл для загрузки
        /// </summary>
        string FileName { get; set; }
    }
}
