namespace HW_7.Interfaces
{
    public interface IBenchmark
    {
        bool RunBenchmark();

        int ThreadsCount {get;set;}
    }
}