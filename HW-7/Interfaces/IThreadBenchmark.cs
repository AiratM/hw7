using System;
using System.Collections.Generic;
namespace HW_7.Interfaces
{
    public interface IThreadBenchmark
    {
        bool RunBenchmark(out TimeSpan elapsedTime, out List<string> errorMessages);
    }
}