﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HW_7.Entities;

namespace HW_7.Interfaces
{
    public interface IClientParser
    {
        /// <summary>
        /// Парсинг строки в объект
        /// </summary>
        /// <param name="str">Строка для парсинга</param>
        /// <returns>Объект Клиент</returns>
        Client Parse(string str);

    }
}
