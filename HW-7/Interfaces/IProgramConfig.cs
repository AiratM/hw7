namespace HW_7.Interfaces
{
    public interface IProgramConfig
    {
        /// <summary>
        /// Количество записей в файле
        /// </summary>
        int itemsCount { get; set; }
        
        /// <summary>
        /// Количество потоков для обработки в тесте ThreadBenchmark
        /// </summary>
        int threadsCount { get; set; }
        
        /// <summary>
        /// Файл для записи/чтения
        /// </summary>
        string fileName { get; set; }
        
        /// <summary>
        /// Запускаемые тесты
        /// </summary>
        BenchmarkConfig benchmarkConfig { get; set; }
    }
}