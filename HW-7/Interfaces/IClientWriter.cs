﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HW_7.Entities;

namespace HW_7.Interfaces
{
    public interface IClientWriter
    {
        /// <summary>
        /// Сохранение массива клиентов в файл
        /// </summary>
        /// <param name="clients">Массив для сохранения</param>
        /// <returns>true - OK</returns>
        bool WriteClients(ref List<Client> clients);

    }
}
