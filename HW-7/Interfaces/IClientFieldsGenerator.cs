﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_7.Interfaces
{
    public interface IClientFieldsGenerator
    {
        string GenerateFullName();

        string GenerateEmail();

        string GeneratePhoneNumber();
    }
}
