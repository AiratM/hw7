using System;
namespace HW_7.Interfaces
{
    public interface IConsoleMessageFormatter
    {
        string FormatDate(TimeSpan ts);
    }
}