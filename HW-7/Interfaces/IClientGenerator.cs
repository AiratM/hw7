﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HW_7.Entities;

namespace HW_7.Interfaces
{
    public interface IClientGenerator
    {
        /// <summary>
        /// Генерация списка клиентов
        /// </summary>
        /// <param name="count">Количество клиентов</param>
        /// <param name="clientsList">Результирующий массив</param>
        /// <returns>true - OK</returns>
        bool GenerateClients(out List<Client> clientsList);
    }
}
