using System;
using System.Collections.Generic;
namespace HW_7.Interfaces
{
    public interface IThreadPoolBenchmark
    {
        bool RunBenchmark(out TimeSpan elapsedTime, out List<string> errorMessages);
    }
}