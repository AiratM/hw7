using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using HW_7.Interfaces;
using HW_7.Entities;

namespace HW_7.Services
{
    public class ThreadBenchmarkService : IThreadBenchmark
    {
        private IProgramConfig _programConfig;
        private int _linesToThread;

        private object _lockObject= new Object();

        private ConcurrentDictionary<int, Client> _readed = new();

        private List<string> _errors = new();
        public ThreadBenchmarkService(IProgramConfig programConfig)
        {
            _programConfig = programConfig ?? throw new NullReferenceException(nameof(IProgramConfig));
        }
        public bool RunBenchmark(out TimeSpan elapsedTime, out List<string> errorMessages)
        {
            Stopwatch sw = new();
            sw.Start();
            _linesToThread = _programConfig.itemsCount / _programConfig.threadsCount;
            Thread[] threads = new Thread[_programConfig.threadsCount];
            for (int i = 0; i < threads.Length; i++)
            {
                threads[i] = new Thread(ParseFile);
                threads[i].Name = $"FileParser {i}";
                threads[i].Start(_linesToThread * i);
            }
            for (int i = 0; i < threads.Length; i++)
            {
                threads[i].Join();
            }

            errorMessages = _errors;
            sw.Stop();
            elapsedTime = sw.Elapsed;
            return true;
        }

        private Client parseCsvString(string s)
        {
            var splitted = s.Split(';');
            if (splitted.Count()!=4)
            throw new FormatException("Ошибка парсинга файла! Несоответствие количества элементов в строке");
            return new Client() { 
                Id = int.Parse(splitted[0].Trim()),
                FullName = splitted[1].Trim(),
                Email = splitted[2].Trim(),
                PhoneNumber = splitted[3].Trim()
             };
        }
        public void ParseFile(object startLine)
        {
            Console.WriteLine($"Старт парсинга файла в потоке {Thread.CurrentThread.Name} со строки {startLine}");
            try
            {
                var data = File.ReadLines(_programConfig.fileName).Skip((int)startLine).Take(_linesToThread);
                Console.WriteLine($"{Thread.CurrentThread.Name}: Прочитано ok");
                foreach (var s in data)
                {
                    Client c = parseCsvString(s);
                    _readed.TryAdd(c.Id, c);
                }
                Console.WriteLine($"Парсинг файла в потоке {Thread.CurrentThread.Name} успешно завершен");

            }
            catch (Exception e)
            {
                lock (_lockObject)
                {
                    _errors.Add($"Error {e.Message} on {e.Source}. Stacktrace: {e.StackTrace}. Thread is {Thread.CurrentThread.Name}");
                }
            }

        }
    }
}