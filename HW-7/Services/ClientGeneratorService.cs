﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HW_7.Interfaces;
using HW_7.Entities;

namespace HW_7.Services
{
    public class ClientGeneratorService : IClientGenerator
    {
        private readonly IClientFieldsGenerator _clientFieldsGenerator;
        private IProgramConfig _programConfig;
        public ClientGeneratorService(IClientFieldsGenerator clientFieldsGenerator, IProgramConfig programConfig)
        {
            _clientFieldsGenerator = clientFieldsGenerator ?? throw new NullReferenceException(nameof(IClientFieldsGenerator));
            _programConfig = programConfig ?? throw new NullReferenceException(nameof(IProgramConfig));
        }
        public bool GenerateClients(out List<Client> clientsList)
        {
            clientsList = new List<Client> { Capacity = _programConfig.itemsCount + 1 };
            for (int i = 0; i < _programConfig.itemsCount; i++)
            {
                clientsList.Add(new Client
                {
                    Id = i,
                    FullName = _clientFieldsGenerator.GenerateFullName(),
                    Email = _clientFieldsGenerator.GenerateEmail(),
                    PhoneNumber = _clientFieldsGenerator.GeneratePhoneNumber()
                });
            }
            return true;
        }


    }
}
