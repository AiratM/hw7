using HW_7.Interfaces;
namespace HW_7.Services
{
    public class ProgramConfigService : IProgramConfig
    {
        /// <inheritdoc/>
        public int itemsCount{get;set;}

        public int threadsCount {get;set;}

        public string fileName{get;set;}    

        public BenchmarkConfig benchmarkConfig {get;set;}

    }
}