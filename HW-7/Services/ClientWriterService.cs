﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HW_7.Entities;
using HW_7.Interfaces;

namespace HW_7.Services
{
    public class ClientWriterService : IClientWriter
    {
        private IProgramConfig _programConfig;

        public ClientWriterService(IProgramConfig programConfig)
        {
            _programConfig = programConfig ?? throw new NullReferenceException(nameof(IProgramConfig));
        }
        public bool WriteClients(ref List<Client> clients)
        {
            if (_programConfig.fileName is null || _programConfig.fileName == string.Empty)
                _programConfig.fileName = "data.csv";
            using (StreamWriter sw = new StreamWriter(_programConfig.fileName))
            {
                foreach (var item in clients)
                {
                    sw.WriteLine(item);
                }

            }
            return true;
        }

    }
}
