﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HW_7.Interfaces;

namespace HW_7.Services
{
    public class ClientFieldsGeneratorService: IClientFieldsGenerator
    {
        private static readonly string alphabet = "abcdefghijklmnopqrstuvwxyz";
        private static readonly string digits = "1234567890";
        private static readonly Random rnd = new ();

        public string GenerateFullName()
        {
            return $"{GenerateString(rnd.Next(3,12))} {GenerateString(rnd.Next(3, 12))} {GenerateString(rnd.Next(3, 12))}";
        }

        public string GenerateEmail()
        {
            return $"{GenerateString(rnd.Next(3, 12))}@{GenerateString(rnd.Next(3, 12))}.{GenerateString(rnd.Next(2, 4))}";
        }

        public string GeneratePhoneNumber()
        {
            return GenerateDigitString();
        }

        private static string GenerateDigitString(int length = 11)
        {
            return Enumerable.Range(0, length).Select(x => digits[rnd.Next(0,10)].ToString()).Aggregate((a, b) => a + b);
        }


        private static string GenerateString(int length)
        {
            return Enumerable.Range(0, length).Select(x => alphabet[rnd.Next(0,26)].ToString()).Aggregate((a, b) => a + b);
        }
    }
}
