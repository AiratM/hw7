using System;
using HW_7.Interfaces;
namespace HW_7.Services
{
    public class ConsoleMessageFormatterService : IConsoleMessageFormatter
    {
        public string FormatDate(TimeSpan ts) => string.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);
    }
}