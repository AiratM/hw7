using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using HW_7.Interfaces;
using HW_7.Entities;

namespace HW_7.Services
{
    public class ThreadPoolBenchmarkService : IThreadPoolBenchmark
    {
        private IProgramConfig _programConfig;
        private int _linesToThread;

        private object _lockObject = new Object();

        private ConcurrentDictionary<int, Client> _readed = new();

        private ManualResetEvent[] _doneEvents;

        private List<string> _errors = new();
        public ThreadPoolBenchmarkService(IProgramConfig programConfig)
        {
            _programConfig = programConfig ?? throw new NullReferenceException(nameof(IProgramConfig));
        }
        public bool RunBenchmark(out TimeSpan elapsedTime, out List<string> errorMessages)
        {
            Stopwatch sw = new();
            sw.Start();
            _doneEvents = new ManualResetEvent[_programConfig.threadsCount];
            _linesToThread = _programConfig.itemsCount / _programConfig.threadsCount;
            for (int i = 0; i < _programConfig.threadsCount; i++)
            {
                _doneEvents[i] = new ManualResetEvent(false);
                ThreadPool.QueueUserWorkItem(ParseFile, i);
            }
            WaitHandle.WaitAll(_doneEvents);
            errorMessages = _errors;
            sw.Stop();
            elapsedTime = sw.Elapsed;
            return true;
        }

        private Client parseCsvString(string s)
        {
            var splitted = s.Split(';');
            if (splitted.Count() != 4)
                throw new FormatException("Ошибка парсинга файла! Несоответствие количества элементов в строке");
            return new Client()
            {
                Id = int.Parse(splitted[0].Trim()),
                FullName = splitted[1].Trim(),
                Email = splitted[2].Trim(),
                PhoneNumber = splitted[3].Trim()
            };
        }
        public void ParseFile(object index)
        {
            int startLine = _linesToThread * (int)index;
            Console.WriteLine($"Старт парсинга файла в потоке {Thread.CurrentThread.Name} со строки {startLine}");
            try
            {
                var data = File.ReadLines(_programConfig.fileName).Skip((int)startLine).Take(_linesToThread);
                Console.WriteLine($"{Thread.CurrentThread.ManagedThreadId}: Прочитано ok");
                foreach (var s in data)
                {
                    Client c = parseCsvString(s);
                    _readed.TryAdd(c.Id, c);
                }
                _doneEvents[(int)index].Set();
                Console.WriteLine($"Парсинг файла в потоке {Thread.CurrentThread.ManagedThreadId} успешно завершен");


            }
            catch (Exception e)
            {
                lock (_lockObject)
                {
                    _errors.Add($"Error {e.Message} on {e.Source}. Stacktrace: {e.StackTrace}. Thread is {Thread.CurrentThread.Name}");
                }
            }

        }
    }
}