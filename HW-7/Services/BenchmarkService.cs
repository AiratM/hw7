using System;
using System.Collections.Generic;
using HW_7.Interfaces;
using HW_7.Entities;

namespace HW_7.Services
{
    public class BenchmarkService : IBenchmark
    {
        private IThreadBenchmark _threadBenchmark;

        private IThreadPoolBenchmark _threadPoolBenchmark;
        private IConsoleMessageFormatter _msgFormatter;
        private IProgramConfig _programConfig;

        public int ThreadsCount { get; set; }
        public BenchmarkService(IConsoleMessageFormatter messageFormatter, IThreadBenchmark threadBenchmark, IThreadPoolBenchmark threadPoolBenchmark,
                                IProgramConfig programConfig)
        {
            _msgFormatter = messageFormatter ?? throw new NullReferenceException(nameof(IConsoleMessageFormatter));
            _threadBenchmark = threadBenchmark ?? throw new NullReferenceException(nameof(IThreadBenchmark));
            _threadPoolBenchmark = threadPoolBenchmark ?? throw new NullReferenceException(nameof(IThreadPoolBenchmark));
            _programConfig = programConfig ?? throw new NullReferenceException(nameof(IProgramConfig));
        }

        public bool RunThreadBenchmark()
        {
            if (_threadBenchmark.RunBenchmark(out TimeSpan elapsedTime, out List<string> errorMessages))
            {
                Console.WriteLine(@$"Успешно завершено ThreadBenchmark. Время выполнения:{_msgFormatter.FormatDate(elapsedTime)}. 
                                            Количество ошибок {errorMessages.Count}");
                foreach (var item in errorMessages)
                {
                    Console.WriteLine(item);
                }
                return true;
            }
            Console.WriteLine("Ошибка при выполнении");
            foreach (var item in errorMessages)
            {
                Console.WriteLine(item);
            }
            return false;
        }

        public bool RunThreadPoolBenchmark()
        {
            if (_threadPoolBenchmark.RunBenchmark(out TimeSpan elapsedTime, out List<string> errorMessages))
            {
                Console.WriteLine(@$"Успешно завершено ThreadPoolBenchmark. Время выполнения:{_msgFormatter.FormatDate(elapsedTime)}. 
                                            Количество ошибок {errorMessages.Count}");
                foreach (var item in errorMessages)
                {
                    Console.WriteLine(item);
                }
                return true;
            }
            Console.WriteLine("Ошибка при выполнении");
            foreach (var item in errorMessages)
            {
                Console.WriteLine(item);
            }
            return false;
        }
        public bool RunBenchmark()
        {
            bool res = false;
            if (_programConfig.benchmarkConfig == BenchmarkConfig.RunThreadBenchmark ||
                    _programConfig.benchmarkConfig == BenchmarkConfig.RunAllBenchmarks)
            {
                res = RunThreadBenchmark();
            }
            if (_programConfig.benchmarkConfig == BenchmarkConfig.RunThreadPoolBenchmark ||
                    _programConfig.benchmarkConfig == BenchmarkConfig.RunAllBenchmarks)
            {
                res = RunThreadPoolBenchmark();
            }
            return res;
        }
    }
}