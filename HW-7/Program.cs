﻿using System;
using Microsoft.Extensions.DependencyInjection;
using HW_7.Interfaces;
using HW_7.Entities;
using HW_7.Services;
using System.Collections.Generic;
using System.Diagnostics;

namespace HW_7
{
    class Program
    {
        static int Main(string[] args)
        {
            var services = new ServiceCollection()
                .AddSingleton<IClientFieldsGenerator, ClientFieldsGeneratorService>()
                .AddSingleton<IClientWriter, ClientWriterService>()
                .AddSingleton<IConsoleMessageFormatter, ConsoleMessageFormatterService>()
                .AddSingleton<IProgramConfig, ProgramConfigService>()
                .AddScoped<IClientGenerator, ClientGeneratorService>()
                .AddScoped<IBenchmark, BenchmarkService>()
                .AddScoped<IThreadBenchmark, ThreadBenchmarkService>()
                .AddScoped<IThreadPoolBenchmark, ThreadPoolBenchmarkService>();
            var serviceProvider = services.BuildServiceProvider();

            var clientsGenerator = serviceProvider.GetService<IClientGenerator>() ?? throw new NullReferenceException(nameof(IClientGenerator));
            var clientsWriter = serviceProvider.GetService<IClientWriter>() ?? throw new NullReferenceException(nameof(IClientWriter));
            var msgFormatter = serviceProvider.GetService<IConsoleMessageFormatter>() ?? throw new NullReferenceException(nameof(IConsoleMessageFormatter));
            var benchmarkService = serviceProvider.GetService<IBenchmark>() ?? throw new NullReferenceException(nameof(IBenchmark));
            var config = serviceProvider.GetService<IProgramConfig>() ?? throw new NullReferenceException(nameof(IProgramConfig));
            
            #region Configuration
            config.itemsCount = 1000000;
            config.benchmarkConfig = BenchmarkConfig.RunThreadBenchmark;
            config.fileName = "data.csv";
            config.threadsCount = 64;
            #endregion

            #region Generate_data
            Stopwatch stopWatch = new();
            Console.WriteLine("Запуск генерации списка клиентов!");
            stopWatch.Start();
            clientsGenerator.GenerateClients(out List<Client> lstClients);
            stopWatch.Stop();
            Console.WriteLine($"Сгенерировано за: {msgFormatter.FormatDate(stopWatch.Elapsed)}");
            var res = clientsWriter.WriteClients(ref lstClients) == true ? "Успешно сохранено" : "Ошибка при сохранении";
            Console.WriteLine(res);
            #endregion

            benchmarkService.RunBenchmark();

            return 0;
        }
    }
}
