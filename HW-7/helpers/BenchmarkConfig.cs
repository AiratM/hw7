namespace HW_7
{
    public enum BenchmarkConfig
    {
        RunThreadBenchmark,
        RunThreadPoolBenchmark,
        RunAllBenchmarks
    }
}