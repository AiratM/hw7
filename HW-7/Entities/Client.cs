﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW_7.Entities
{
    public class Client
    {
        /// <summary>
        /// ID клиента
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// ФИО клиента
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Email клиента
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Номер телефона клиента
        /// </summary>
        public string PhoneNumber { get; set; }

        public override string ToString()
        {
            return $"{Id}; {FullName}; {Email}; {PhoneNumber}";
        }
    }
}
